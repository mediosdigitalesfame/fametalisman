<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Refacciones</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="FAME Talismán BMW · MINI Morelia, Michoacán. Sitio WEB oficial FAME Talismán Morelia. Te brindamos información sobre las mejores marcas de lujo del mercado. BMW y MINI Talismán te invitan a conocer la exclusiva gama de vehículos y todos los servicios que ofrecemos para tí. Piensa en auto, piensa en FAME. ">
    <meta name="keywords" content="FAME Talisman Morelia, bmw morelia, bmw michoacan, bmw mexico, bmw talisman, agencia bmw morelia, agencia mini morelia, seminuevos bmw morelia, usados bmw morelia, usados mini morelia, fame morelia, fame talisman, grupo fame bmw, grupo fame mini, mini en morelia, bmw en morelia, servicio bmw morelia, servicio mini morelia, servicio fame, autos nuevos morelia, autos seminuevos morelia, servicio fame talisman, servicio talisman, agencia bmw morelia, agencia mini morelia, bmw morelia seminuevos, mini morelia seminuevos, bmw serie 1 morelia, bmw serie 2 morelia, bmw serie 3 morelia, precio bmw morelia, precio mini morelia, mini cooper morelia, autos mini morelia, autos mini cooper morelia, vendo mini morelia, vendo bmw morelia, mini michoacan, mini cooper michoacan, precio mini morelia, agencia mini telefono, agencia mini ubicacion, agencia mimi cooper morelia, mini convertible, mini 3 puertas, mini 5 puertas, mini cooper 3 puertas, mini cooper 5 puertas, nuevo mini 2015, mini cooper 2015, mini cooper coupe, mini cooper convertible, mini cooper roadster, mini cooper countryman, mini cooper paceman, john cooper, john cooper works, mini john cooper, camioneta mini, camioneta mini cooper, autos bmw, autos mini, camioneta bmw, camioneta bmw seminueva, camioneta bmw morelia, camioneta bmw venta, bmw serie i, bmw serie i 2015, bmw serie 1, bmw serie 1 2015, bmw serie 1 tres puertas, bmw serie 1 3 puertas, bmw serie 1 5 puertas, bmw serie 1 cinco puertas, bmw serie 1 nuevo, bmw serie 1 seminuevo, bmw serie 1 morelia, bmw serie 2, bmw serie 2 morelia, bmw serie 2 usado, bmw serie 2 coupe, bmw serie 3, bmw serie 3 sedan, bmw serie 3 active, bmw serie 3 active hybrid, bmw hibrido, nuevo bmw 2015, mini talisman, bmw talisman, taller mini morelia, taller mini cooper morelia, taller bmw, taller bmw morelia, taller bmw mexico, taller talisman, taller talisman morelia, talisman automotriz, fame automotriz, 2015, 2014, agencia grupo fame, agencia fame, servicio fame, servicio fame morelia, taller fame, taller fame morelia, servicio autos de lujo, servicio autos de lujo morelia, morelia, michoacan, mexico, autos usados morelia, autos nuevos morelia, lote autos morelia, concesionaria bmw, concesionaria bmw morelia, concesionaria mini, concesionaria mini morelia, distribuidor mini, distribuidor mini morelia, distribuidor bmw, distribuidor bmw morelia">
    <meta name="author" content="Alejandro Cruz Saucedo">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    

    <link rel="icon" type="image/png" href="/images/favicon.png" />
    
    <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
</script>

</head>
<body>

	<!-- Container -->
	<div id="container">
    
		<!-- Header -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							<span><i class="fa fa-phone"></i>Agencia: (443) 324 7200</span>
						</p>
						<ul class="social-icons">
                        	<li><a class="facebook" href="redes.html" target="_self"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a class="twitter" href="redes.html" target="_self"><i class="fa fa-twitter-square"></i></a></li>
			    <li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a class="whatsapp" href="http://www.grupofame.com/whatsapp/" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            
						</ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="index.html">Inicio</a></li>
							<li class="drop"><a>Vehículos</a>
								<ul class="drop-down">
									<li><a href="autos-bmw.html" target="_self">BMW®</a></li>
									<li><a href="autos-mini.html" target="_self">MINI®</a></li>
                                </ul> </li>    

							<li class="drop"><a class="active" href="servicio.php">Servicio</a>
								<ul class="drop-down">
									<li><a href="servicio.php">Cita de Servicio</a></li>
                                    <li><a href="servicio-bmw.html" target="_self">Servicio BMW®</a></li>
                                    <li><a href="servicio-mini.html" target="_self">Servicio MINI®</a></li>
                                    <li><a href="refacciones.php">Refacciones</a></li>
                                    <li><a href="garantia-bmw.html">Garantía BMW®</a></li>
                                    <li><a href="garantia-mini.html">Garantía MINI®</a></li>
                                  </ul> </li> 
                                  
							<li class="drop"><a href="promociones.html">Promociones</a>
                            	<ul class="drop-down">
                                    <li><a href="financiamiento-bmw.html">Financiamiento BMW®</a></li>
                                    <li><a href="financiamiento-mini.html">Financiamiento MINI®</a></li>
                            	</ul></li>        
                                         
                            <li><a href="http://www.fameseminuevos.com/" target="_blank">Seminuevos</a></li>      
							<li class="drop"><a href="contacto.php">Contacto</a>
                            	<ul class="drop-down">
                                	<li><a href="redes.html" target="_self">Redes Sociales</a></li>
                                	<li><a href="contacto.php">Contacto</a></li>
                                    <li><a href="manejo.php">Cita de Manejo</a></li>
                                    <li><a href="servicio.php">Cita de Servicio</a></li>
                                    <li><a href="cotiza.php">Cotizador de auto</a></li>
                                </ul></li>
                             <li><a href="ubicacion.html" target="_blank">Ubicación</a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>


		<!-- End Header -->
        
<!-- ANALYTICS TALISMÁN-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47756123-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- FIN ANALYTICS -->    

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Refacciones y Accesorios</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
                
							<div class="single-project-content">
								<img alt="" src="images/talisman-refacciones.jpg">
                         	</div>                

						<div class="col-md-12" align="center">
							<h3>Cotiza tu refacción</h3>
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "refacciones@fametalisman.com" . ','. "formas@grupofame.com" . ','. "gerencia@fametalisman.com";
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Refacciones FAME Talismán";
			$asunto = "Cotizador Refacciones FAME Talismán";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <br><br><br>

<?php if(isset($result)) { echo $result; } ?>
    </form>
						</div>
                
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">




				<div class="welcome-box">
					<div class="container">
                    	<div class="col-md-6">
                
						<h1><span>Refacciones Originales BMW®</span></h1><br>
						<p align="justify"><strong>Satisfacción garantizada.</strong><br><br>

La seguridad es una cualidad esencial en un automóvil de calidad. Por ello los ingenieros de BMW recomiendan utilizar únicamente, Refacciones Originales BMW cuando su BMW lo necesite. Estas piezas cumplen los mismos estrictos estándares de calidad que los nuevos componentes que incluía su vehículo cuando lo compró. Lo cual supone elegir la máxima seguridad y calidad en el producto, además de una compatibilidad optima, que se traduce en una perfecta armonía en el funcionamiento de todas las piezas de su BMW. <br><br>

Las Refacciones Originales BMW juegan un papel vital a la hora de asegurar su disfrute durante la conducción. Ciertas piezas, como los frenos o algunos componentes del chasis, son esenciales para lograr la seguridad y dinamismo que caracterizan a un BMW.</p><br><br>
						</div>
                        
                        <div class="col-md-6">
                        <h1><span>Refacciones Originales MINI®</span></h1>
<p align="justify"><strong>Keep It MINI.</strong></p><br>
<p align="justify">Cuando tengas que sustituir alguna pieza de tu MINI, confía en los Refacciones Originales MINI. Pues sólo así mantendrás exactamente la misma calidad que en un MINI nuevo, además de la garantía. Tu MINI seguirá siendo un verdadero MINI al 100%, lo conducirás como un verdadero MINI… y podrás venderlo como un verdadero MINI.<br>
</p><br><br>

<p align="justify"><strong>Disponibles 15 años, finalizada la producción.</strong></p><br>
<p align="justify">En cada taller autorizado MINI de todo el mundo encontrarás todo el surtido de refacciones para todos los modelos MINI hasta 15 años después del fin de la producción del modelo correspondiente. Y la larga disponibilidad no es la única ventaja de los Refacciones Originales MINI nuevos<br>
</p><br><br>

<br><br>


					</div>
<br>


                	</div>
				</div>
 
               </div>
             </div>
 



		<!-- footer -->
		<footer>
			<div class="footer-line">
				<div class="container">
					<p><span><i class="fa fa-phone"></i> 01800 670 8386 | </span> 2016 Fame Talisman  | <i class="fa fa-user"> </i><a href="aviso.html" target="_self"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>
		<!-- End footer -->
        
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>

</body>
</html>